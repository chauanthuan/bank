@extends('layouts.voucher')

@section('content')
<?php //$vc_gotit = false;?>
		<div id="voucher_wrapper" class="has_custom">
			<div class="voucher_header">
				<a class="logo" href="#"><img src="https://www.techcombank.com.vn/Content/images/logo.png"></a>

				<div class="language_fs">
                    <?php
                        $browserLang = Request::server('HTTP_ACCEPT_LANGUAGE');
                        $langPos = strpos($browserLang, 'vi');

                        if(!$langPos){
                            $browserLang = 'en';
                        }else{
                            $browserLang = 'vi';
                        }

                        $lang = Cookie::get('laravel_language', $browserLang);
                    ?>
                    @if( $lang == "vi")
                        <p><img src='../layouts/v2/images/lang-vi.png' style='width:25px !important;vertical-align:middle;'/></p>
                    @else
                        <p><img src='../layouts/v2/images/lang-en.png' style='width:25px !important;vertical-align:middle;'/></p>
                    @endif
                    <div class="drop_language" style="display:none;">
                        <a href="javascript:void(0)" class="" data-lang="vi"><img src='../layouts/v2/images/lang-vi.png' style='width:25px !important;vertical-align:middle;'/>VI</a>
                        <a href="javascript:void(0)" class="" data-lang="en"><img src='../layouts/v2/images/lang-en.png' style='width:25px !important;vertical-align:middle;'/>EN</a>
                    </div>
                </div>
			</div>
			<div class="voucher_body" style="position:relative">

				<!-- Check if is Normal voucher -->
				@if(!$vc_gotit)
					@if($voucher_tp != null)
						<div class="vc_template" style="">
							<img src="{{env('IMG_SERVER').env('AWS_VOUCHER_TEMPLATE_FOLDER').'/'.$voucher_tp->top_image}}" height="" width="100%" style="margin:0 auto !important;display:block;">
						</div>
					@endif
				<div class="voucher_info">
					<div class="img">
						@if($voucher->state == 4)
							<p style="position:absolute;
										top:10px;
										right:10px;
										padding:5px 10px;
										background:#ff9500;
										color:#fff;
										font-weight:bold;
										font-size:14px;
										z-index:9;
										font-family:Roboto;
										border-radius:4px;
										">
										{{trans('content.used')}}</p>
						@endif
						@if($voucher->state == 8)
							<p style="position:absolute;
								top:10px;
								right:10px;
								padding:5px 10px;
								background:#FF5F5F;
								color:#fff;
								font-weight:bold;
								font-size:14px;
								z-index:9;
								font-family:Roboto;
								border-radius:4px;
								">
							{{trans('content.v_expired')}}</p>
						@endif
						<img src="{{ Image::show($voucher->img_path)  }}">
					</div>
					<div class="cl"></div>
					<div class="product_info">
						
						<div class="detail">
							<div class="brand_logo">
								<img src="{{ Image::show($product->brand->logo_img->img_path) }}" alt="">
							</div>
							<h3>{!! Translate::transObj($product, 'name')   !!}</h3>
							@if($psize != null)
								<p>{{$psize}}</p>
							@endif
							@if($voucher->receiver_name != null)
								<p style="font-size: 14px;font-weight: bold;padding-top: 10px;">{{($lang == "vi") ? 'Thân tặng Quý Khách Hàng' : 'Dear Mr/Mrs'}}<br>{{$voucher->receiver_name}}</p>
							@endif
						</div>
						<div class="cl"></div>
						<div class="barcode">
							<?php //dd($voucher)?>
							
							@if($disableBarcode)
		                        <p class="barcode disable">
		                            <span>{{ trans('content.voucher_fb')}}</span>
		                            @if(!isset($loginUrl))
				                    <a class="loginfb_btn" href="javascript:void(0)" onClick="loginFb();"><i class="fa fa-facebook-square"></i>Login Facebook</a>
				                    @else
				                    <a href="{{ $loginUrl }}">
				                        <img src="../layouts/v2/images/voucher/fblogin-button.png" alt="" style="opacity: 1;">
				                    </a>
				                    @endif
		                        </p>
		                    @else
		                    	<!-- <p class="notice">{!! trans('content.v_notice') !!}</p> -->
		                    	<p class="notice">
		                    	@if($lang == 'vi')
		                    		Vui lòng cung cấp Mã Code<br> để sử dụng dịch vụ tại danh sách <a class='goto_list_st' href='javascript:void(0)'>cửa hàng</a>
		                    	@else
		                    		Please provide below code to redeem <br>at <a class='goto_list_st' href='javascript:void(0)'>stores</a>
		                    	@endif
		                    	</p>
		                    	<!-- <div class="qrCode" style="">
		                    		<img src="data:image/png;base64, {!! base64_encode(QrCode::format('png')->margin(1)->size(80)->generate($voucher->code)) !!} ">
		                    	</div>
		                    	<div class="textCode" style="bottom:initial;margin-top:5px;">
		                    		<p class="code">
			                            <?php echo implode(" " , array(substr($voucher->code, 0,3),substr($voucher->code, 3,3),substr($voucher->code, 6,4) ));?>
			                        </p>
			                        <p>{{ trans('content.v_validity').": ".date('d/m/Y', strtotime($voucher->expired_date))}}</p>
		                    	</div> -->
		                    	@include('voucher.code_section')	
		                    @endif
						</div>
						@if($voucher->state != 4 &&  $voucher->state != 8)
							<div class="phontopup">
								<h3>{{trans('content.or')}}</h3>
								<p>{{$lang == 'vi' ? 'Nhập số điện thoại bạn muốn nạp tiền':'Please enter phone number you want to top up'}}</p>
								<div class="logo_phone">
									<img src="{{env('IMG_SERVER')}}compress/brand/2016/08/1471921408_KxS6U.png">
									<img src="{{env('IMG_SERVER')}}compress/brand/2016/08/1471920885_6tISg.png">
									<img src="{{env('IMG_SERVER')}}compress/brand/2016/08/1471921291_yINyr.png">
								</div>
								<div class="form-topup">
									<div class="error_phone" style="color:#ff5f5f;"></div>
	                                <input type="hidden" value="{{$voucher->code}}" name="code">
	                                <input class="phone_number" name="phone_number" value="" type="tel" placeholder="{{trans('content.phone_number')}}" autocomplete="off" minlength="10" maxlength="11">
	                                <a class="send_ant_btn" href="javascript:void(0)">{{$lang=="vi" ? "Gửi":"Send"}}</a>
								</div>
							</div>
						@endif
					</div>
				</div>
				@else
				<!-- Check if is gotit voucher -->
				<div class="voucher_info gotit_voucher_n">
					@if($voucher_tp != null)
					<div class="vc_template" style="">
						<img src="{{env('IMG_SERVER').env('AWS_VOUCHER_TEMPLATE_FOLDER').'/'.$voucher_tp->top_image}}" height="" width="100%" style="margin:0 auto !important;display:block;">
					</div>
					@endif
					<div class="img">
						@if($voucher->state == 4)
							<p style="position:absolute;
										top:10px;
										right:10px;
										padding:5px 10px;
										background:#ff9500;
										color:#fff;
										font-weight:bold;
										font-size:14px;
										z-index:9;
										font-family:Roboto;
										border-radius:4px;
										">
										{{trans('content.used')}}</p>
						@endif
						@if($voucher->state == 8)
							<p style="position:absolute;
								top:10px;
								right:10px;
								padding:5px 10px;
								background:#FF5F5F;
								color:#fff;
								font-weight:bold;
								font-size:14px;
								z-index:9;
								font-family:Roboto;
								border-radius:4px;
								">
							{{trans('content.v_expired')}}</p>
						@endif
						<!-- <img src="../layouts/v2/images/mvoucher/gotit_voucher.png"> -->
						<img src="{{ Image::show($voucher->img_path)  }}">
						
					</div>
					<div class="all_brand_n">
						<!-- <p>{{ trans('content.redeemalble_store',['Num_store'=>$num_brand->total])}}</p> -->
						@if($list_brand != "")
						<div class="list">
							<div class="wrap_img">
							@foreach($list_brand as $brand)
								<img src="{{ Image::show($brand->img_path) }}" data-id="{{$brand->brand_id}}"/>
							@endforeach
							</div>
							@if($lang == "vi")
								<p class="extanal_link">Xem thông tin chi tiết <a href="http://techcombank-popup.gotit.vn/">thương hiệu</a></p>
							@else
								<p class="extanal_link">For more details about <a href="http://techcombank-popup.gotit.vn/">brands</a></p>
							@endif
						</div>
						@endif
						<!-- <img src="../layouts/v2/images/mvoucher/all_brand.png"> -->
					</div>
					<!-- <h3 class="see_all_brand">{{ trans('content.see_all_brand')}}</h3> -->
					<div class="cl"></div>
					<div class="product_info">
						
						<div class="detail">
							<!-- <div class="brand_logo">
								<img src="{{ Image::show($product->brand->logo_img->img_path) }}" alt="">
							</div> -->
							<h3>{!! Translate::transObj($product, 'name')   !!}</h3>
							@if($psize != null)
								<p>{{$psize}}</p>
							@endif
							@if($voucher->receiver_name != null)
								<p style="font-size: 14px;font-weight: bold;padding-top: 10px;">{{($lang == "vi") ? 'Thân tặng Quý Khách Hàng' : 'Dear Mr/Mrs'}}<br>{{$voucher->receiver_name}}</p>
							@endif
						</div>
						<div class="cl"></div>
						<div class="barcode">
							<?php //dd($voucher)?>
							
							@if($disableBarcode)
		                        <p class="barcode disable">
		                            <span>{{ trans('content.voucher_fb')}}</span>
		                            @if(!isset($loginUrl))
				                    <a class="loginfb_btn" href="javascript:void(0)" onClick="loginFb();"><i class="fa fa-facebook-square"></i>Login Facebook</a>
				                    @else
				                    <a href="{{ $loginUrl }}">
				                        <img src="../layouts/v2/images/voucher/fblogin-button.png" alt="" style="opacity: 1;">
				                    </a>
				                    @endif
		                        </p>
		                    @else
		                    	<!-- <p class="notice">{!! trans('content.v_notice') !!}</p> -->
		                    	<p class="notice">
		                    	@if($lang == 'vi')
		                    		Vui lòng cung cấp Mã Code<br> để sử dụng dịch vụ tại danh sách <a class='our_location' href='javascript:void(0)'>cửa hàng</a>
		                    	@else
		                    		Please provide below code to redeem<br> at <a class='our_location' href='javascript:void(0)'>stores</a>
		                    	@endif
		                    	</p>
		                    	<!-- <div class="qrCode">
		                    		<img src="data:image/png;base64, {!! base64_encode(QrCode::format('png')->margin(1)->size(80)->generate($voucher->code)) !!} ">
		                    	</div>
		                    	<div class="textCode" style="bottom:initial;margin-top:5px;">
		                    		<p class="code">
			                            {{-- $voucher->code --}}
			                            <?php echo implode(" " , array(substr($voucher->code, 0,3),substr($voucher->code, 3,3),substr($voucher->code, 6,4) ));?>
			                        </p>
			                        <p>{{ trans('content.v_validity').": ".date('d/m/Y', strtotime($voucher->expired_date))}}</p>
			                        
		                    	</div> -->

		                    	@include('voucher.code_section')
		                    @endif
						</div>
						@if($voucher->state != 4 &&  $voucher->state != 8)
							<div class="phontopup">
								<h3>{{trans('content.or')}}</h3>
								<p>{{$lang == 'vi' ? 'Nhập số điện thoại bạn muốn nạp tiền':'Please enter phone number you want to top up'}}</p>
								<div class="logo_phone">
									<img src="{{env('IMG_SERVER')}}compress/brand/2016/08/1471921408_KxS6U.png">
									<img src="{{env('IMG_SERVER')}}compress/brand/2016/08/1471920885_6tISg.png">
									<img src="{{env('IMG_SERVER')}}compress/brand/2016/08/1471921291_yINyr.png">
								</div>
								<div class="form-topup">
									<div class="error_phone" style="color:#ff5f5f;"></div>
	                                <input type="hidden" value="{{$voucher->code}}" name="code">
	                                <input class="phone_number" name="phone_number" value="" type="tel" placeholder="{{trans('content.phone_number')}}" autocomplete="off" minlength="10" maxlength="11">
	                                <a class="send_ant_btn" href="javascript:void(0)">{{$lang == "vi" ? "Gửi":"Send"}}</a>
								</div>
							</div>
						@endif
					</div>
				</div>
				@endif
				<!-- End voucher info -->
<style type="text/css">
.extanal_link{
	font-size: 14px;
    font-weight: normal;
    font-style: normal;
    font-stretch: normal;
    line-height: 1.5;
    text-align: center;
    color: #333333;
}
.extanal_link a{
	color: #ff5f5f;
}
.phontopup{
	text-align: center;
}
.phontopup h3{
	margin-top: 10px; 
	text-align: center;
	text-transform:uppercase;
}
.phontopup p{
	font-size: 14px;
	text-align: center;
}
.phontopup .logo_phone{
	width: 100%;
	text-align: center;
}
.logo_phone img{
	width: 70px;
	height: 70px;
}
.form-topup{
	text-align: center;
}
#voucher_wrapper .voucher_body .voucher_info.gotit_voucher_n .img img{
	top: 0 !important;
	transform:initial !important;
}
#voucher_wrapper .voucher_body .voucher_info .product_info .barcode .notice{
	font-size: 13px !important;
}
</style>



				<div class="acor-info-vc voucher_note">
					<ul>
						<li>
							<h2>{{ trans('content.product_description') }}</h2>
							<div>
		                        <p>{!! Translate::transObj($product, 'desc') !!}</p>
		                        <!-- <h4>{{ trans('content.how_it_work') }}</h4>
		                        <p>{!! Translate::transObj($product, 'service_guide') !!}</p>
		                        <br>
		                        <h4>{{ trans('content.brand') }} : <a href="/brand/{{ $product->brand->name_slug }}">{{ Translate::transObj($product->brand, 'name') }}</a></h4>
		                        <p>{{ trans('content.phone') }} : {{ $product->brand->phone }}</p>
		                        <p>{{ trans('content.address') }} : {{ Translate::transObj($product->brand, 'address') }}</p> -->

		                        <h3 class="viewMap find_store"><img src="/layouts/v2/images/mvoucher/map_icon.png" height="20px"><a href="#" class="list_btn" data-brandid="{{$voucher->brand_id}}" data-storegroupid ="{{ $product->store_group_id }}">{{ trans('content.find_near')}}</a> &nbsp;</h3>
		                    </div>
						</li>
						<!-- <li>
							<h2 class="viewMap"><a href="javascript:void(0)">{{ trans('content.show_store')}}</a></h2>
						</li> -->
						<li>
							<h2>{{ trans('content.important')}}</h2>
							<div>
                        		<p>{{ trans('content.important_text')}}</p>
							</div>
						</li>
						<li>
		                    <h2>{{ trans('content.term_and_condition') }}</h2>
		                    <div>
		                        <p>{!! Translate::transObj($product, 'note') !!}</p>
		                    </div>
		                </li>
		                <li>
							<a href="{{env('WEB_LINK')}}/about.html"><h2>{{ trans('content.more_gotit')}}</h2></a>
						</li>
					</ul>
				</div>
				
				<div class="send_again">
					<p>{{ trans('content.gift_easy')}}</p>
                    <a class="send_gift" target="_blank" href="{{env('WEB_LINK')}}/product.html">{{ trans('content.send_a_gift')}}</a>

					<p>{{ trans('content.save_use_late') }}</p>
                    {{--<a class="save_img" href="@if(isset($disableBarcode) && $disableBarcode == false) {{ env('IMG_SERVER').env('AWS_VOUCHER_FOLDER').'/'.$link.'-'.md5($voucher->voucher_id.$voucher->code).'_'.Session::get('laravel_language').'.png' }} @endif" @if(isset($disableBarcode) && $disableBarcode == false) {{ 'download="voucher_'.$link.'"' }} @endif >{{ trans('content.save_voucher') }}</a>--}}
                    <a class="save_img" href="@if(isset($disableBarcode) && $disableBarcode == false) {{ route('voucher.save',['code'=>Crypt::encrypt($link.'|'.$voucher->voucher_id.$voucher->code),'lang'=>$lang]) }} @endif" @if(isset($disableBarcode) && $disableBarcode == false) @endif >{{ trans('content.save_voucher') }}</a>
				</div>
			</div>
			<!-- Google map -->
			<div class="store">
		        <div class="map-wrap" id="store_location">
		            <div class="map active">
		            	<!-- <div class="voucher_header">
							<a class="logo" href="{{env('WEB_LINK')}}"><img src="../layouts/v2/images/mvoucher/logo_header.png" ></a>
							<a href="{{ env('WEB_LINK') }}">{{trans('content.what_gotit')}}</a>
							<div class="language_fs">
			                    <?php
			                        $browserLang = Request::server('HTTP_ACCEPT_LANGUAGE');
			                        $langPos = strpos($browserLang, 'vi');

			                        if(!$langPos){
			                            $browserLang = 'en';
			                        }else{
			                            $browserLang = 'vi';
			                        }

			                        $lang = Cookie::get('laravel_language', $browserLang);
			                    ?>
			                    @if( $lang == "vi")
			                        <p><img src='../layouts/v2/images/lang-vi.png' style='width:25px !important;vertical-align:middle;'/></p>
			                    @else
			                        <p><img src='../layouts/v2/images/lang-en.png' style='width:25px !important;vertical-align:middle;'/></p>
			                    @endif
			                    <div class="drop_language" style="display:none;">
			                        <a href="javascript:void(0)" class="" data-lang="vi"><img src='../layouts/v2/images/lang-vi.png' style='width:25px !important;vertical-align:middle;'/> VI</a>
			                        <a href="javascript:void(0)" class="" data-lang="en"><img src='../layouts/v2/images/lang-en.png' style='width:25px !important;vertical-align:middle;'/> EN</a>
			                    </div>
			                </div>
						</div> -->
		            	<div class="top_map">
							<h3>
								<img src="{{ Image::show($product->brand->logo_img->img_path) }}" alt="">
								<?php echo mb_strimwidth($voucher->brand_name, 0, 25, "...", 'UTF-8');?>
								{{--$voucher->brand_name--}}
							</h3>
							<span class="back_voucher"></span>
							@if($vc_gotit)
								<span class="show_more_brand"></span>
							@endif
		            	</div>
		            	<div class="find_store">
                            <input name="brand_id_choose" type="hidden" value="">
		            		<h3><img src="/layouts/v2/images/mvoucher/map_icon.png" height="20px"><a href="javascript:void(0)" class="find" data-brandid="{{$voucher->brand_id}}" data-storegroupid ="{{ $product->store_group_id }}">{{ trans('content.find_near')}}</a></h3>
		            	</div>
		            	<div id="embed-map"></div>
		                <div class="add-wrap">
		                    <div class="list-add">
		                        <input type="hidden" name="store_group_id" value="{{ $product->store_group_id }}">
		                        <ul>
		                            <?php
		                            $number = 0;
		                            ?>
		                            @foreach($stores as $store)

		                            <li>
		                                <p class="goto-location" data-number="<?php echo $number ?>" style="cursor: pointer;">
		                                    {{ Translate::transObj($store, 'brand_name') . " - " . Translate::transObj($store, 'name') }}<br/>
		                                    <span>{{ Translate::transObj($store, 'address') }}</span><br>
		                                    <span>{{ trans('content.phone')}}: {{$store->phone}}</span>
		                                    <?php $browser = strtolower($_SERVER['HTTP_USER_AGENT']);?>
											@if(stripos($browser,'iphone') !== false || stripos($browser,'ipad') !== false)
											  	<!-- <a href="comgooglemaps://?q={{Translate::transObj($store,'address')}}&center={{$store->lat}},{{$store->long}}"> -->
												<a href="comgooglemaps://?daddr={{Translate::transObj($store,'address')}}&directionsmode=driving">
											@else
												<!-- <a href="geo:{{$store->lat}},{{$store->long}}?q={{Translate::transObj($store,'address')}}"> -->
												<a href="google.navigation:q={{Translate::transObj($store,'address')}}&{{$store->lat}},{{$store->long}}">
											@endif
											<img src="../layouts/v2/images/mvoucher/direct.png"></a>
		                                </p>
		                            </li>
		                            <?php
		                            $number++;
		                            ?>
		                            @endforeach
		                        </ul>
		                    </div>
		                </div>
		                
		            </div>
		        </div>
			</div>

			<div class="brand_ls_voucher active">
				<!-- <div class="voucher_header">
					<a class="logo" href="{{ env('WEB_LINK') }}"><img src="../layouts/v2/images/mvoucher/logo_header.png"></a>
					<a href="{{ env('WEB_LINK') }}">{{trans('content.what_gotit')}}</a>
					<div class="language_fs">
	                    <?php
	                        $browserLang = Request::server('HTTP_ACCEPT_LANGUAGE');
	                        $langPos = strpos($browserLang, 'vi');

	                        if(!$langPos){
	                            $browserLang = 'en';
	                        }else{
	                            $browserLang = 'vi';
	                        }

	                        $lang = Cookie::get('laravel_language', $browserLang);
	                    ?>
	                    @if( $lang == "vi")
	                        <p><img src='../layouts/v2/images/lang-vi.png' style='width:25px !important;vertical-align:middle;'/></p>
	                    @else
	                        <p><img src='../layouts/v2/images/lang-en.png' style='width:25px !important;vertical-align:middle;'/></p>
	                    @endif
	                    <div class="drop_language" style="display:none;">
	                        <a href="javascript:void(0)" class="" data-lang="vi"><img src='../layouts/v2/images/lang-vi.png' style='width:25px !important;vertical-align:middle;'/> VI</a>
	                        <a href="javascript:void(0)" class="" data-lang="en"><img src='../layouts/v2/images/lang-en.png' style='width:25px !important;vertical-align:middle;'/> EN</a>
	                    </div>
	                </div>
				</div> -->
				<div class="top_brand">
					<h3>{{ trans('content.all_brand')}}</h3>
					<span class="back_store"></span>
				</div>
				<div class="band_list">
					<p>{{ trans('content.choose_brand')}}</p>
					@if($list_brand != "")
						<input type="hidden" name="store_group_id" value="{{$product->store_group_id}}">
						@foreach($list_brand as $brand)
							<div class="brand_item">
								<a href="javascript:void(0)" data-id = "{{$brand->brand_id}}"><img class="logo-brand" src="{{ Image::show($brand->img_path) }}"/></a>
							</div>
						@endforeach
					@else
						<div class="brand_item">
							<a href="javascript:void(0)" data-id = "{{$voucher->brand_id}}"><img class="logo-brand" src="{{ Image::show($voucher->brand_img_path) }}"/></a>
						</div>
					@endif
				</div>
			</div>
			
			<div class="voucher_footer">
				@if($voucher_tp != null)
					<div class="vc_template_foot" style="">
						<img src="{{env('IMG_SERVER').env('AWS_VOUCHER_TEMPLATE_FOLDER').'/'.$voucher_tp->bottom_image}}" height="" width="100%" style="margin:0 auto !important; display:block;">
					</div>
				@endif
				<div class="footer_info">
					<div class="pull-left">
						<a class="logo_footer" href="{{ env('WEB_LINK') }}"><img src="../layouts/v2/images/mvoucher/logo_header.png" height="32px"></a>
					</div>
					<div class="pull-right">
						<a href="{{ env('WEB_LINK') }}">www.gotit.vn</a> &nbsp;|&nbsp; 1900 5588 20
					</div>
				</div>
			</div>
            <input name="img_server" type="hidden" value="{{ env('IMG_SERVER')}}">
		</div>
<div class="loading">
  <!-- <img src="../layouts/v2/images/loading_icon.gif"> -->
  <div id='img-loading' class='uil-spin-css' style="-webkit-transform:scale(0.4)"><div><div></div></div><div><div></div></div><div><div></div></div><div><div></div></div><div><div></div></div><div><div></div></div><div><div></div></div><div><div></div></div></div>
</div>

<style type="text/css">
.loading {
    position: fixed;
    background: rgba(0, 0, 0, 0.3);
    top: 0px;
    left: 0;
    right: 0;
    bottom: 0;
    z-index: 100;
    display: none;
}
.loading img {
    position: absolute;
    left: 50%;
    top: 50%;
    transform: translate(-50%,-50%);
    width: 80px;
    height: 80px;
}
.send_another{
    margin-top: 15px;
    text-align: center;
}
.send_another p{
    text-align: center;
    margin: 10px;
}
.phone_number{
    border:1px solid #e0e0e0;
    border-radius: 6px;
    padding: 10px;
    font-size: 16px;
    text-align: center;
    font-weight: bold;
    outline: none;
}
.send_ant_btn{
    display: inline-block;
    margin: 10px auto;
    min-width: 100px;
    height: 40px;
    line-height: 40px;
    background: #ff5f5f;
    color: #fff;
    text-align: center;
    border: 0;
    border-radius: 6px;
    text-decoration: none;
}

/*Loading icon*/
#img-loading{
    display: block;
    position: fixed;
    left: 50%;
    top: 50%;
    margin-left: -100px;
    margin-top: -100px;
    z-index:9999;
    /*background: rgba(0, 0, 0, 0.3);
    top: 0px;
    left: 0;
    right: 0;
    bottom: 0;*/
}
#img-loading img{
    width: 75px;
    height: 75px;
}
.uil-spin-css {
  background: none;
  position: relative;
  width: 200px;
  height: 200px;
}
@-webkit-keyframes uil-spin-css {
  0% {
    opacity: 1;
    -webkit-transform: scale(3);
    transform: scale(3);
  }
  100% {
    opacity: 0.1;
    -webkit-transform: scale(1);
    transform: scale(1);
  }
}
@-moz-keyframes uil-spin-css {
  0% {
    opacity: 1;
    -webkit-transform: scale(3);
    transform: scale(3);
  }
  100% {
    opacity: 0.1;
    -webkit-transform: scale(1);
    transform: scale(1);
  }
}
@-webkit-keyframes uil-spin-css {
  0% {
    opacity: 1;
    -webkit-transform: scale(3);
    transform: scale(3);
  }
  100% {
    opacity: 0.1;
    -webkit-transform: scale(1);
    transform: scale(1);
  }
}
@-o-keyframes uil-spin-css {
  0% {
    opacity: 1;
    -webkit-transform: scale(3);
    transform: scale(3);
  }
  100% {
    opacity: 0.1;
    -webkit-transform: scale(1);
    transform: scale(1);
  }
}
@keyframes uil-spin-css {
  0% {
    opacity: 1;
    -webkit-transform: scale(3);
    transform: scale(3);
  }
  100% {
    opacity: 0.1;
    -webkit-transform: scale(1);
    transform: scale(1);
  }
}
.uil-spin-css > div {
  width: 16px;
  height: 16px;
  margin-left: 8px;
  margin-top: 8px;
  position: absolute;
}
.uil-spin-css > div > div {
  width: 100%;
  height: 100%;
  border-radius: 100px;
  background: #ff5f5f;
}
.uil-spin-css > div:nth-of-type(1) > div {
  -webkit-animation: uil-spin-css 1s linear infinite;
  animation: uil-spin-css 1s linear infinite;
  -webkit-animation-delay: -0.87s;
  animation-delay: -0.87s;
}
.uil-spin-css > div:nth-of-type(1) {
  -webkit-transform: translate(84px, 84px) rotate(45deg) translate(70px, 0);
  transform: translate(84px, 84px) rotate(45deg) translate(70px, 0);
}
.uil-spin-css > div:nth-of-type(2) > div {
  -webkit-animation: uil-spin-css 1s linear infinite;
  animation: uil-spin-css 1s linear infinite;
  -webkit-animation-delay: -0.75s;
  animation-delay: -0.75s;
}
.uil-spin-css > div:nth-of-type(2) {
  -webkit-transform: translate(84px, 84px) rotate(90deg) translate(70px, 0);
  transform: translate(84px, 84px) rotate(90deg) translate(70px, 0);
}
.uil-spin-css > div:nth-of-type(3) > div {
  -webkit-animation: uil-spin-css 1s linear infinite;
  animation: uil-spin-css 1s linear infinite;
  -webkit-animation-delay: -0.62s;
  animation-delay: -0.62s;
}
.uil-spin-css > div:nth-of-type(3) {
  -webkit-transform: translate(84px, 84px) rotate(135deg) translate(70px, 0);
  transform: translate(84px, 84px) rotate(135deg) translate(70px, 0);
}
.uil-spin-css > div:nth-of-type(4) > div {
  -webkit-animation: uil-spin-css 1s linear infinite;
  animation: uil-spin-css 1s linear infinite;
  -webkit-animation-delay: -0.5s;
  animation-delay: -0.5s;
}
.uil-spin-css > div:nth-of-type(4) {
  -webkit-transform: translate(84px, 84px) rotate(180deg) translate(70px, 0);
  transform: translate(84px, 84px) rotate(180deg) translate(70px, 0);
}
.uil-spin-css > div:nth-of-type(5) > div {
  -webkit-animation: uil-spin-css 1s linear infinite;
  animation: uil-spin-css 1s linear infinite;
  -webkit-animation-delay: -0.37s;
  animation-delay: -0.37s;
}
.uil-spin-css > div:nth-of-type(5) {
  -webkit-transform: translate(84px, 84px) rotate(225deg) translate(70px, 0);
  transform: translate(84px, 84px) rotate(225deg) translate(70px, 0);
}
.uil-spin-css > div:nth-of-type(6) > div {
  -webkit-animation: uil-spin-css 1s linear infinite;
  animation: uil-spin-css 1s linear infinite;
  -webkit-animation-delay: -0.25s;
  animation-delay: -0.25s;
}
.uil-spin-css > div:nth-of-type(6) {
  -webkit-transform: translate(84px, 84px) rotate(270deg) translate(70px, 0);
  transform: translate(84px, 84px) rotate(270deg) translate(70px, 0);
}
.uil-spin-css > div:nth-of-type(7) > div {
  -webkit-animation: uil-spin-css 1s linear infinite;
  animation: uil-spin-css 1s linear infinite;
  -webkit-animation-delay: -0.12s;
  animation-delay: -0.12s;
}
.uil-spin-css > div:nth-of-type(7) {
  -webkit-transform: translate(84px, 84px) rotate(315deg) translate(70px, 0);
  transform: translate(84px, 84px) rotate(315deg) translate(70px, 0);
}
.uil-spin-css > div:nth-of-type(8) > div {
  -webkit-animation: uil-spin-css 1s linear infinite;
  animation: uil-spin-css 1s linear infinite;
  -webkit-animation-delay: -0s;
  animation-delay: -0s;
}
.uil-spin-css > div:nth-of-type(8) {
  -webkit-transform: translate(84px, 84px) rotate(360deg) translate(70px, 0);
  transform: translate(84px, 84px) rotate(360deg) translate(70px, 0);
}

</style><script type="text/javascript">
    <?php
    $googleMaps = array();
    foreach ($stores as $store) {
        $googleMaps[] = array(
            'name'  => Translate::transObj($store, 'name'),
            'brand_name'  => Translate::transObj($store, 'brand_name'),
            'lat'   => $store->lat,
            'lng'   => $store->long,
            'address'   =>  Translate::transObj($store, 'address'),
            'phone' => $store->phone
        );
    }
    echo 'var mapStores = '.json_encode($googleMaps);
    ?>
</script>
<script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyCjbeyHo86kWXc3nDZYK8q4AW5Joi0mvOY&v=3.exp&libraries=places"></script>
<script type="text/javascript">
	$(document).ready(function(){
    $('.send_ant_btn').click(function(){
        
        var lang = $('html').attr('lang');
        var phone_val = $('input[name="phone_number"]').val();
        //var amount = $('input[name="price_value"]').val();
        //var amount = '10000';
        var code = $('input[name="code"]').val();

        var err = '';
        if(phone_val.length == 0){
            if(lang == 'vi'){
                err += '<p>Vui lòng nhập số điện thoại</p>';
            }
            else{
                err += '<p>Please enter the phone number</p>';
            }
            
        }
        else if(phone_val.length > 11 || phone_val.length < 10){
            if(lang == 'vi'){
                err += '<p>Số điện thoại không được ít hơn 10 số hoặc nhiều hơn 11 số</p>';
            }
            else{
                err += '<p>Phone numbers must not be less than 10 digits or more than 11 digits</p>';
            }
            
        }
        else{
            var regExp = /^(09|08|01[2689])\d{8}$/; 
            var checkphone = phone_val.match(regExp);
            if (!checkphone) {
                if(lang == 'vi'){
                    err += '<p>Số điện thoại không hợp lệ, vui lòng kiểm tra lại</p>';
                }
                else{
                    err += '<p>Phone number is invalid, please check again</p>';
                }
            }
        }

        if(err != '' || err.length > 0){
            $('.error_phone').html('').append(err);
            return false;
        }
        else{
            $('.error_phone').html('');
            $('.loading').css({'display':'block'});
            // $('#img-loading').show();
            var GotitRequest = {
                phoneNumber:phone_val,code:code
            };

            var hasResult = false;

            $.ajax({
                url:'<?php echo env('PHONE_TOPUP_URL','https://topup.gotit.vn:9898');?>/topup',//call lumen project
                async: true,
                crossDomain: true,
                type:'POST',
                dataType:'json',
                contentType: "application/json; charset=utf-8",
                headers: {
                    "Content-Type": "application/json"
                },
                data: JSON.stringify(GotitRequest),
                timeout:30000,
                success:function(data){
                    var msg = '';
                    if(lang == 'vi'){
                        if(data.stt == -2 || data.stt == -1){
                            msg = 'Voucher không hợp lệ';
                        }
                        else if(data.stt == 0){
                            msg = 'Voucher đã sử dụng';
                        }
                        else if(data.stt == 1){
                            msg = 'Nạp tiền thành công';
                        }
                        else if(data.stt == 3){
                            msg = 'Nạp tiền thất bại';
                        }
                        else if(data.stt == 2){
                            msg = 'Đang xử lý quá trình nạp tiền';
                        }
                        else if(data.stt == 4){
                            msg = 'Quá nhiều yêu cầu từ máy bạn. Vui lòng tải lại trang và thử lại sau khoảng 1 phút.';
                        }
                    }
                    else{
                        if(data.stt == -2 || data.stt == -1){
                            msg = 'Voucher Invalid';
                        }
                        else if(data.stt == 0){
                            msg = 'Voucher has used';
                        }
                        else if(data.stt == 1){
                            msg = 'Topup card success';
                        }
                        else if(data.stt == 3){
                            msg = 'Topup card fail';
                        }
                        else if(data.stt == 2){
                            msg = 'Topup card processing';
                        }
                        else if(data.stt == 4){
                            msg = 'Too many request, please refresh page and try again in a minuste.';
                        }
                    }
                    
                    $('.phontopup').html('').append('<p><b>'+msg+'</b></p>');

                    if(data.stt != 2){
                        hasResult = true;
                    }
                },

                error: function (xhr, ajaxOptions, thrownError) {
					console.log(xhr.status);
					console.log(ajaxOptions);
					console.log(thrownError);

                    if(ajaxOptions==="timeout") {
                        $('.loading').css({'display':'none'});

                        if(lang == 'vi')
                            msg = 'Đang xử lý quá trình nạp tiền';
                        else
                            msg = 'Topup card processing';
                        $('.phontopup').html('').append('<p><b>'+msg+'</b></p>');

                        if(!hasResult)
                            checktransacion(code, lang);
                    }
                }
           }).done(function(){
                // $('#img-loading').hide();
                $('.loading').css({'display':'none'});
                if(!hasResult)
                    checktransacion(code, lang);
            });
        }
    });
});


var loop = 30;
var ind = 0;
//loop for 30 times
function checktransacion(code, lang){
	var hasResult = false;

	var request = {
		code:code
	};

	setTimeout(function() {
		if (ind <= loop) {
			ind++;

			$.ajax({
				url: '<?php echo env('PHONE_TOPUP_URL','https://topup.gotit.vn:9898');?>/checkprocessing',//call lumen project
				async: true,
				crossDomain: true,
				type: 'POST',
				dataType: 'json',
				contentType: "application/json; charset=utf-8",
				headers: {
					"Content-Type": "application/json"
				},
				data: JSON.stringify(request),

				success: function (data) {
					console.log(data);
					if(lang == 'vi'){
						if (data.stt == 0) {
							msg = 'Nạp tiền thành công';
						}
						else if (data.stt != 99 && data.stt != 100) {
							msg = 'Nạp tiền thất bại';
						}
					}
					else {
						if (data.stt == 0) {
							msg = 'Topup phone successfully';
						}
						else if (data.stt != 99 && data.stt != 100) {
							msg = 'Topup phone failed';
						}
					}

					if (data.stt == 0 || (data.stt != 99 && data.stt != 100)) {
						$('.phontopup').html('').append('<p><b>' + msg + '</b></p>');
						ind = loop + 1;//break the loop
					}
				},

				error: function (xhr, ajaxOptions, thrownError) {
					console.log(xhr.status);
					console.log(ajaxOptions);
					console.log(thrownError);
				}
			});

			checktransacion(code);
		}
	}, 5000);
}
</script>
@endsection