@extends('layouts.voucher')

@section('content')
<style>

</style>
<?php //dd($voucher);?>
	<?php //$vc_gotit = false;?>
	<div id="voucher_wrapper" class="techcombank">
		<div class="voucher_header">
			<a class="logo" href="#"><img src="https://www.techcombank.com.vn/Content/images/logo.png"></a>
			
			<div class="language_fs">
				<?php
				$browserLang = Request::server('HTTP_ACCEPT_LANGUAGE');
				$langPos = strpos($browserLang, 'vi');

				if(!$langPos){
					$browserLang = 'en';
				}else{
					$browserLang = 'vi';
				}

				$lang = Cookie::get('laravel_language', $browserLang);
				?>
				@if( $lang == "vi")
					<p><img src='../layouts/v2/images/lang-vi.png' style='width:25px !important;vertical-align:middle;'/></p>
				@else
					<p><img src='../layouts/v2/images/lang-en.png' style='width:25px !important;vertical-align:middle;'/></p>
				@endif
				<div class="drop_language" style="display:none;">
					<a href="javascript:void(0)" class="" data-lang="vi"><img src='../layouts/v2/images/lang-vi.png' style='width:25px !important;vertical-align:middle;'/>VI</a>
					<a href="javascript:void(0)" class="" data-lang="en"><img src='../layouts/v2/images/lang-en.png' style='width:25px !important;vertical-align:middle;'/>EN</a>
				</div>
			</div>
		</div>
		<div class="voucher_body" style="position:relative">
			<!-- Check if is Normal voucher -->
			@if(!$vc_gotit)
				@if($voucher_tp != null)
					<div class="vc_template" style="">
						<img src="{{env('IMG_SERVER').env('AWS_VOUCHER_TEMPLATE_FOLDER').'/'.$voucher_tp->top_image}}" height="" width="100%" style="margin:0 auto !important;display:block;">
					</div>
				@endif
				<div class="voucher_info">
					<div class="img">
						@if($voucher->state == 4)
							<p style="position:absolute;
										top:10px;
										right:10px;
										padding:5px 10px;
										background:#ff9500;
										color:#fff;
										font-family: Roboto;
										font-size: 14px;
										font-weight:bold;
										font-size:18px;
										z-index:9;
										border-radius:4px;
										">
								{{trans('content.used')}}</p>
						@endif
						@if($voucher->state == 8)
							<p style="position:absolute;
								top:10px;
								right:10px;
								padding:5px 10px;
								background:#FF5F5F;
								color:#fff;
								z-index:9;
								font-family: Roboto;
								font-size: 14px;
								font-weight: bold;
								text-align: right;
								color: #ffffff;
								border-radius:4px;
								">
								{{trans('content.v_expired')}}</p>
						@endif
						<img src="{{ Image::show($voucher->img_path)  }}">
					</div>
					<div class="cl"></div>
					<div class="product_info">

						<div class="detail">
							<div class="brand_logo">
								<img src="{{ Image::show($product->brand->logo_img->img_path) }}" alt="">
							</div>
							<!-- <h3>{{$voucher->brand_name}}</h3> -->
							<h3>{{ Translate::transObj($product, 'name')   }}</h3>
							@if($psize != null)
								<p>{{$psize}}</p>
							@endif

							@if($voucher->receiver_name != null)
								<p style="font-size: 14px;font-weight: bold;padding-top: 10px;">{{trans('content.dear_receiver')}}<br>{{$voucher->receiver_name}}</p>
							@endif
						</div>
						<div class="cl"></div>
						<div class="barcode">
							<?php //dd($voucher)?>

							@if($disableBarcode)
							<p class="barcode disable">
								<span>{{ trans('content.voucher_fb')}}</span>
								@if(!isset($loginUrl))
								<a class="loginfb_btn" href="javascript:void(0)" onClick="loginFb();"><i class="fa fa-facebook-square"></i>Login Facebook</a>
								@else
								<a href="{{ $loginUrl }}">
									<img src="../layouts/v2/images/voucher/fblogin-button.png" alt="" style="opacity: 1;">
								</a>
								@endif
							</p>
							@else
								<p class="notice">
									@if($lang == "vi")
		                    		Cung cấp mã code tại <a class="goto_list_st" href="javascript:void(0)">cửa hàng</a><br> để sử dụng
		                    		@else
		                    		Show this code to staff <br>at our <a class="goto_list_st" href="javascript:void(0)">brand stores</a>
		                    		@endif
								</p>
								<!-- <div class="qrCode" style="">
		                    		<img src="data:image/png;base64, {!! base64_encode(QrCode::format('png')->margin(1)->size(80)->generate($voucher->code)) !!} ">
		                    	</div>
		                    	<div class="textCode" style="">
		                    		<p class="code">
			                            <?php echo implode(" " , array(substr($voucher->code, 0,3),substr($voucher->code, 3,3),substr($voucher->code, 6,4) ));?>
			                        </p>
			                        <p>{{ trans('content.v_validity').": ".date('d/m/Y', strtotime($voucher->expired_date))}}</p>
		                    	</div> -->
		                    	@include('voucher.code_section')
							@endif
						</div>
					</div>
				</div>
		@else
			<!-- Check if is gotit voucher -->
				<div class="voucher_info gotit_voucher_n">
					@if($voucher_tp != null)
						<div class="vc_template" style="">
							<img src="{{env('IMG_SERVER').env('AWS_VOUCHER_TEMPLATE_FOLDER').'/'.$voucher_tp->top_image}}" height="" width="100%" style="margin:0 auto !important;display:block;">
						</div>
					@endif
					<div class="img">
						@if($voucher->state == 4)
							<p style="position:absolute;
										top:10px;
										right:10px;
										padding:5px 10px;
										background:#ff9500;
										color:#fff;
										font-weight:bold;
										font-size:14px;
										border-radius:4px;
										z-index:9;
										">
								{{trans('content.used')}}</p>
						@endif
						@if($voucher->state == 8)
							<p style="position:absolute;
								top:10px;
								right:10px;
								padding:5px 10px;
								background:#FF5F5F;
								color:#fff;
								font-weight:bold;
								font-size:14px;
								border-radius:4px;
								z-index:9;
								">
								{{trans('content.v_expired')}}</p>
					@endif
					<!-- <img src="../layouts/v2/images/mvoucher/gotit_voucher.png"> -->
						<img src="{{ Image::show($voucher->img_path)  }}">
						
					</div>
					<?php $code =base64_encode(base64_encode($voucher->code."@@hoayeuthuong"));?>
					<div class="all_brand_n">
						<!-- <p>{{ trans('content.redeemalble_store',['Num_store'=>$num_brand->total])}}</p> -->
						@if($list_brand != "")
							<div class="list">
								<div class="wrap_img">
									@foreach($list_brand as $brand)
										<img class="" src="{{ Image::show($brand->img_path) }}" data-id="{{$brand->brand_id}}" @if($brand->brand_id == 93) style="display:none" @endif/>
									@endforeach
									<a href="javascript:void(0)" class="hyt"><img src="../layouts/v2/images/logo_hoayeuthuong.png"></a>
								</div>
							</div>
					@endif
					<!-- <img src="../layouts/v2/images/mvoucher/all_brand.png"> -->
					</div>
					<!-- <h3 class="see_all_brand">{{ trans('content.see_all_brand')}}</h3> -->
					<div class="cl"></div>
					<div class="product_info">

						<div class="detail">
							<!-- <div class="brand_logo">
								<img src="{{ Image::show($product->brand->logo_img->img_path) }}" alt="">
							</div> -->
							<!-- <h3>{{$voucher->brand_name}}</h3> -->
							<h3>{{ Translate::transObj($product, 'name')   }}</h3>
							@if($psize != null)
								<p>{{$psize}}</p>
							@endif

							@if($voucher->receiver_name != null)
								<p style="font-size: 14px;font-weight: bold;padding-top: 10px;">{{trans('content.dear_receiver')}}<br>{{$voucher->receiver_name}}</p>
							@endif
						</div>
						<div class="cl"></div>
						<div class="barcode">
							<?php //dd($voucher)?>

							@if($disableBarcode)
							<p class="barcode disable">
								<span>{{ trans('content.voucher_fb')}}</span>
								@if(!isset($loginUrl))
								<a class="loginfb_btn" href="javascript:void(0)" onClick="loginFb();"><i class="fa fa-facebook-square"></i>Login Facebook</a>
								@else
								<a href="{{ $loginUrl }}">
									<img src="../layouts/v2/images/voucher/fblogin-button.png" alt="" style="opacity: 1;">
								</a>
								@endif
							</p>
							@else
								<p class="notice">{!! trans('content.v_notice') !!}</p>
								<!-- <div class="qrCode" style="">
		                    		<img src="data:image/png;base64, {!! base64_encode(QrCode::format('png')->margin(1)->size(80)->generate($voucher->code)) !!} ">
		                    	</div>
		                    	<div class="textCode" style="">
		                    		<p class="code">
			                            <?php echo implode(" " , array(substr($voucher->code, 0,3),substr($voucher->code, 3,3),substr($voucher->code, 6,4) ));?>
			                        </p>
			                        <p>{{ trans('content.v_validity').": ".date('d/m/Y', strtotime($voucher->expired_date))}}</p>
		                    	</div> -->
		                    	@include('voucher.code_section')
							@endif
						</div>
					</div>
				</div>
		@endif
		<!-- End voucher info -->



			<div class="acor-info-vc voucher_note">
				<ul>
					<li>
						<h2>{{ trans('content.product_description') }}</h2>
						<div>
							<p>{!! Translate::transObj($product, 'desc') !!}</p>
						<!-- <h4>{{ trans('content.how_it_work') }}</h4>
		                        <p>{!! Translate::transObj($product, 'service_guide') !!}</p>
		                        <br>
		                        <h4>{{ trans('content.brand') }} : <a href="/brand/{{ $product->brand->name_slug }}">{{ Translate::transObj($product->brand, 'name') }}</a></h4>
		                        <p>{{ trans('content.phone') }} : {{ $product->brand->phone }}</p>
		                        <p>{{ trans('content.address') }} : {{ Translate::transObj($product->brand, 'address') }}</p> -->

							<h3 class="viewMap find_store"><img src="/layouts/v2/images/mvoucher/map_icon.png" height="20px"><a href="#" class="list_btn" data-brandid="{{$voucher->brand_id}}" data-storegroupid ="{{ $product->store_group_id }}">{{ trans('content.find_near')}}</a> &nbsp;</h3>
						</div>
					</li>
				<!-- <li>
							<h2 class="viewMap"><a href="javascript:void(0)">{{ trans('content.show_store')}}</a></h2>
						</li> -->
					<li>
						<h2>{{ trans('content.important')}}</h2>
						<div>
							<p>{{ trans('content.important_text')}}</p>
						</div>
					</li>
					<li>
						<h2>{{ trans('content.term_and_condition') }}</h2>
						<div>
							<p>{!! Translate::transObj($product, 'note') !!}</p>
						</div>
					</li>
					
				</ul>
			</div>

			<div class="send_again">
				<p>{{ trans('content.save_use_late') }}</p>
				{{--<a class="save_img" href="@if(isset($disableBarcode) && $disableBarcode == false) {{ env('IMG_SERVER').env('AWS_VOUCHER_FOLDER').'/'.$link.'-'.md5($voucher->voucher_id.$voucher->code).'_'.Session::get('laravel_language').'.png' }} @endif" @if(isset($disableBarcode) && $disableBarcode == false) {{ 'download="voucher_'.$link.'"' }} @endif >{{ trans('content.save_voucher') }}</a>--}}
				<a class="save_img" href="@if(isset($disableBarcode) && $disableBarcode == false) {{ route('voucher.save',['code'=>Crypt::encrypt($link.'|'.$voucher->voucher_id.$voucher->code),'lang'=>$lang]) }} @endif" @if(isset($disableBarcode) && $disableBarcode == false) download @endif >{{ trans('content.save_voucher') }}</a>
			</div>
		</div>
		<!-- Google map -->
		<div class="store">
			<div class="map-wrap" id="store_location">
				<div class="map active">
					<div class="voucher_header">
						<a class="logo" href="{{env('WEB_LINK')}}"><img src="../layouts/v2/images/mvoucher/logo_header.png" ></a>
						<a href="{{env('WEB_LINK')}}">{{trans('content.what_gotit')}}</a>
						<div class="language_fs">
							<?php
							$browserLang = Request::server('HTTP_ACCEPT_LANGUAGE');
							$langPos = strpos($browserLang, 'vi');

							if(!$langPos){
							$browserLang = 'en';
							}else{
							$browserLang = 'vi';
							}

							$lang = Cookie::get('laravel_language', $browserLang);
							?>
							@if( $lang == "vi")
								<p><img src='../layouts/v2/images/lang-vi.png' style='width:25px !important;vertical-align:middle;'/></p>
							@else
								<p><img src='../layouts/v2/images/lang-en.png' style='width:25px !important;vertical-align:middle;'/></p>
							@endif
							<div class="drop_language" style="display:none;">
								<a href="javascript:void(0)" class="" data-lang="vi"><img src='../layouts/v2/images/lang-vi.png' style='width:25px !important;vertical-align:middle;'/>VI</a>
								<a href="javascript:void(0)" class="" data-lang="en"><img src='../layouts/v2/images/lang-en.png' style='width:25px !important;vertical-align:middle;'/>EN</a>
							</div>
						</div>
					</div>
					<div class="top_map">
						<h3>
							<img src="{{ Image::show($product->brand->logo_img->img_path) }}" alt="">
							<?php echo mb_strimwidth($voucher->brand_name, 0, 25, "...", 'UTF-8');?>
							{{--$voucher->brand_name--}}
						</h3>
						<span class="back_voucher"></span>
						@if($vc_gotit)
							<span class="show_more_brand"></span>
						@endif
					</div>
					<div class="find_store">
						<input name="brand_id_choose" type="hidden" value="">
						<h3><img src="/layouts/v2/images/mvoucher/map_icon.png" height="20px"><a href="#" class="find" data-brandid="{{$voucher->brand_id}}" data-storegroupid ="{{ $product->store_group_id }}">{{ trans('content.find_near')}}</a></h3>
					</div>
					<div id="embed-map"></div>
					<div class="add-wrap">
						<div class="list-add">
							<input type="hidden" name="store_group_id" value="{{ $product->store_group_id }}">
							<ul>
								<?php
								$number = 0;
								?>
								@foreach($stores as $store)
									<?php //dd($store->address_vi)?>
									<li>
										<p class="goto-location" data-number="<?php echo $number ?>" style="cursor: pointer;">
											{{ Translate::transObj($store, 'brand_name') . " - " . Translate::transObj($store, 'name') }}<br/>
											<span>{{ Translate::transObj($store, 'address') }}</span><br>
											<span>{{ trans('content.phone')}}: {{$store->phone}}</span>
										<?php $browser = strtolower($_SERVER['HTTP_USER_AGENT']);?>
										@if(stripos($browser,'iphone') !== false || stripos($browser,'ipad') !== false)
											<!-- <a href="comgooglemaps://?q={{Translate::transObj($store,'address')}}&center={{$store->lat}},{{$store->long}}"> -->
												<a href="comgooglemaps://?daddr={{Translate::transObj($store,'address')}}&directionsmode=driving">
												@else
													<!-- <a href="geo:{{$store->lat}},{{$store->long}}?q={{Translate::transObj($store,'address')}}"> -->
														<a href="google.navigation:q={{Translate::transObj($store,'address')}}&{{$store->lat}},{{$store->long}}">
															@endif
															<img src="../layouts/v2/images/mvoucher/direct.png"></a>
										</p>
									</li>
									<?php
									$number++;
									?>
								@endforeach
							</ul>
						</div>
					</div>

				</div>
			</div>
		</div>

		
		<div class="brand_ls_voucher active">
			<div class="voucher_header">
				<a class="logo" href="{{env('WEB_LINK')}}"><img src="../layouts/v2/images/mvoucher/logo_header.png"></a>
				<a href="{{env('WEB_LINK')}}">{{trans('content.what_gotit')}}</a>
				<div class="language_fs">
					<?php
					$browserLang = Request::server('HTTP_ACCEPT_LANGUAGE');
					$langPos = strpos($browserLang, 'vi');

					if(!$langPos){
					$browserLang = 'en';
					}else{
					$browserLang = 'vi';
					}

					$lang = Cookie::get('laravel_language', $browserLang);
					?>
					@if( $lang == "vi")
						<p><img src='../layouts/v2/images/lang-vi.png' style='width:25px !important;vertical-align:middle;'/></p>
					@else
						<p><img src='../layouts/v2/images/lang-en.png' style='width:25px !important;vertical-align:middle;'/></p>
					@endif
					<div class="drop_language" style="display:none;">
						<a href="javascript:void(0)" class="" data-lang="vi"><img src='../layouts/v2/images/lang-vi.png' style='width:25px !important;vertical-align:middle;'/>VI</a>
						<a href="javascript:void(0)" class="" data-lang="en"><img src='../layouts/v2/images/lang-en.png' style='width:25px !important;vertical-align:middle;'/>EN</a>
					</div>
				</div>
			</div>
			<div class="top_brand">
				<h3>{{ trans('content.all_brand')}}</h3>
				<span class="back_store"></span>
			</div>
			<div class="band_list">
				<p>{{ trans('content.choose_brand')}}</p>
				@if($list_brand != "")
					<input type="hidden" name="store_group_id" value="{{$product->store_group_id}}">
					@foreach($list_brand as $brand)
						<div class="brand_item">
							<a href="javascript:void(0)" data-id = "{{$brand->brand_id}}"><img class="logo-brand" src="{{ Image::show($brand->img_path) }}"/></a>
						</div>
					@endforeach
				@else
					<div class="brand_item">
						<a href="javascript:void(0)" data-id = "{{$voucher->brand_id}}"><img class="logo-brand" src="{{ Image::show($voucher->brand_img_path) }}"/></a>
					</div>
				@endif
			</div>
		</div>

		<div class="hyt_page active">
			<div class="voucher_header">
				<a class="logo" href="{{env('WEB_LINK')}}"><img src="../layouts/v2/images/mvoucher/logo_header.png"></a>
				<a href="{{env('WEB_LINK')}}">{{trans('content.what_gotit')}}</a>
				<div class="language_fs">
					<?php
					$browserLang = Request::server('HTTP_ACCEPT_LANGUAGE');
					$langPos = strpos($browserLang, 'vi');

					if(!$langPos){
					$browserLang = 'en';
					}else{
					$browserLang = 'vi';
					}

					$lang = Cookie::get('laravel_language', $browserLang);
					?>
					@if( $lang == "vi")
						<p><img src='../layouts/v2/images/lang-vi.png' style='width:25px !important;vertical-align:middle;'/></p>
					@else
						<p><img src='../layouts/v2/images/lang-en.png' style='width:25px !important;vertical-align:middle;'/></p>
					@endif
					<div class="drop_language" style="display:none;">
						<a href="javascript:void(0)" class="" data-lang="vi"><img src='../layouts/v2/images/lang-vi.png' style='width:25px !important;vertical-align:middle;'/>VI</a>
						<a href="javascript:void(0)" class="" data-lang="en"><img src='../layouts/v2/images/lang-en.png' style='width:25px !important;vertical-align:middle;'/>EN</a>
					</div>
				</div>
			</div>
			<div class="hyt_content">
				@if(!empty($psize))
					<div class="hyt_background">
						@if( $lang == "vi")
							<p>Mệnh giá bạn đang có: <b>{{$psize}}</b></p>
						@else
							<p>Your e-gift voucher: <b>{{$psize}}</b></p>
						@endif
					</div>
				@endif
				<!-- <p class="code">{{trans('content.voucher_code')}}&nbsp;&nbsp;<b><?php echo implode(" " , array(substr($voucher->code, 0,3),substr($voucher->code, 3,3),substr($voucher->code, 6,4) ));?></b></p> -->
				<p class="notice_text">{!!trans('content.hyt_content')!!}</p>
				<!-- <div class="hyt_form" method="POST" action="http://services.hoayeuthuong.com/Gotit/gotit.ashx"> -->

				<div class="hyt_form">
					<div class="error_phone"></div>
					<input type="text" name="phone" minlength="10" maxlength="11" value="" onkeypress='validate(event)'>
					<input type="hidden" name="voucher_code" value="{{$voucher->code}}">
					<input type="hidden" name="product_id" value="{{$voucher->product_id}}">
					<input type="hidden" name="price_id" value="{{$voucher->price_id}}">
					<a href="javascript:void(0)" class="back_btn">{{trans('content.back')}}</a>
					<button type="button" class="submit_phone" data-productid ="{{$voucher->product_id}}" data-priceid="">{{trans('content.submit')}}</button>
				</div>
				<div class="list_hyt_recommend">
					@if($lang == "vi")
					<p><b>Những mẫu hoa tham khảo</b></p>
					@else
					<p><b>Flowers for your reference</b></p>
					@endif
					<table>
						<tr>
							<td><b>200.000</b></td>
							<td>
								<p class="img"><img src="../layouts/v2/images/list_hoa/3199_yeu-xa.jpg"></p>
								<p class="name">Yêu xa</p>
							</td>
							<td>
								
							</td>
							<td>
								
							</td>
						</tr>
						<tr>
							<td><b>500.000</b></td>
							<td>
								<p class="img"><img src="../layouts/v2/images/list_hoa/4717_nhung-ngay-mong-mo.jpg"></p>
								<p class="name">Những ngày mộng mơ</p>
							</td>
							<td>
								<p class="img"><img src="../layouts/v2/images/list_hoa/4594_du-duong.jpg"></p>
								<p class="name">Du dương</p>
							</td>
							<td>
								<p class="img"><img src="../layouts/v2/images/list_hoa/4720_reu-phong.jpg"></p>
								<p class="name">Rêu phong</p>
							</td>
						</tr>
						<tr>
							<td><b>1.000.000</b></td>
							<td>
								<p class="img"><img src="../layouts/v2/images/list_hoa/2458_princess.jpg"></p>
								<p class="name">Princess</p>
							</td>
							<td>
								<p class="img"><img src="../layouts/v2/images/list_hoa/4718_an-nhien.jpg"></p>
								<p class="name">An nhiên</p>
							</td>
							<td>
								<p class="img"><img src="../layouts/v2/images/list_hoa/2508_100-yeu-em.jpg"></p>
								<p class="name">100% Yêu Em</p>
							</td>
						</tr>
					</table>
				</div>
			</div>
			<div class="voucher_footer">
				<div class="footer_info">
					<div class="pull-left">
						<a class="logo_footer" href="{{env('WEB_LINK')}}"><img src="../layouts/v2/images/mvoucher/logo_header.png" height="32px"></a>
					</div>
					<div class="pull-right">
						<a href="{{env('WEB_LINK')}}">www.gotit.vn</a> &nbsp;|&nbsp; 1900 5588 20
					</div>
				</div>
			</div>
			
		</div>
		
		<div class="voucher_footer">
			@if($voucher_tp != null)
				<div class="vc_template_foot" style="">
					<img src="{{env('IMG_SERVER').env('AWS_VOUCHER_TEMPLATE_FOLDER').'/'.$voucher_tp->bottom_image}}" height="" width="100%" style="margin:0 auto !important; display:block;">
				</div>
			@endif
			<div class="footer_info">
				<div class="pull-left">
					<a class="logo_footer" href="{{env('WEB_LINK')}}"><img src="../layouts/v2/images/mvoucher/logo_header.png" height="32px"></a>
				</div>
				<div class="pull-right">
					<a href="{{env('WEB_LINK')}}">www.gotit.vn</a> &nbsp;|&nbsp; 1900 5588 20
				</div>
			</div>
		</div>
		<input name="img_server" type="hidden" value="{{ env('IMG_SERVER')}}">
	</div>

<!--Modal message-->
<!-- The Modal -->
<div id="noticeModal" class="modal">
  <!-- Modal content -->
  <div class="modal-content">
		<div class="modal-header">
			<h4>{{trans('content.notification')}}</h4>
			<span class="close">&times;</span>
		</div>
		<div class="modal-body">
			<p></p>
		</div>
  </div>
</div>

<style type="text/css">
.hyt_page::-webkit-scrollbar {
    width: 0px;
}
 
.hyt_page::-webkit-scrollbar-track {
    -webkit-box-shadow: inset 0 0 6px rgba(0,0,0,0.3);
}
 
.hyt_page::-webkit-scrollbar-thumb {
  background-color: #e0e0e0;
  outline: 1px solid #e0e0e0;
}
.list_hyt_recommend{
	padding-top: 20px;
}
.list_hyt_recommend table {
	border: none;
	border-collapse:collapse
}
.list_hyt_recommend tr td{
	border-bottom: 1px solid #e0e0e0;
	padding: 5px 0;
}
.list_hyt_recommend tr td:first-child{
	text-align: left;
}
.list_hyt_recommend p.img img{
	width: 80px;
}
.list_hyt_recommend p{
	font-size: 12px;
	text-align: center;
}
#voucher_wrapper.techcombank .voucher_body .voucher_info .all_brand_n .list .wrap_img .slick-track {
    margin: 0 auto;
    width: auto !important;
    text-align: center;
    transform: initial !important;
}
#voucher_wrapper.techcombank .voucher_body .voucher_info .all_brand_n .list .wrap_img .slick-track .slick-slide{
	float: none;
}
#voucher_wrapper.techcombank .voucher_body .voucher_info .all_brand_n .list .wrap_img .slick-track a.slick-slide{
	display: inline-block;
	outline: none;
}
#voucher_wrapper.techcombank .voucher_body .voucher_info .all_brand_n .list .wrap_img .slick-track a.slick-slide:focus{
	outline: none;
}
#voucher_wrapper .hyt_page {
    width: 100%;
    height: 100% !important;
    position: fixed;
    top: 0;
    right: -100%;
    z-index: 10;
    overflow-y: auto;
    margin-top: 0;
    background: #fff;
    max-width: 375px;
    margin: 0 auto;
    /*-webkit-overflow-scrolling: touch;
    background: url(../layouts/v2/images/banner_hyt.png) top 50px center #fff;
    background-repeat: no-repeat;
    background-size: 100% auto;*/
}
#voucher_wrapper .hyt_page .hyt_content{
	padding: 0px 20px 0;
	text-align: center;
	padding-bottom: 70px;
}
#voucher_wrapper .hyt_page .hyt_content .hyt_background{
	height: 50px;
	background: #e0e0e0;
	line-height:50px;
	margin-left:-20px;
	margin-right: -20px;
	/*background: url(../layouts/v2/images/banner_hyt.png) top 0px center #fff;*/
    background-repeat: no-repeat;
    background-size: 100% auto; 
}
.hyt_content p{
	font-size: 14px;
	font-weight: normal;
}
.hyt_content p.code b{
	font-weight: bold;
	font-size: 26px;
}
.hyt_content p.notice_text{
	text-align: center;
	margin: 20px 0 10px;
}
.hyt_content input{
	width: 100%;
	margin: 0 auto;
	display: inline-block;
	border: 1px solid #e0e0e0;
	border-radius: 8px;
	height: 50px;
	line-height: 50px;
	padding: 0px 20px;
	outline: none;
	font-size: 26px;
	font-weight: bold;
	text-align: center;
	-webkit-box-sizing: border-box; /* Safari/Chrome, other WebKit */
    -moz-box-sizing: border-box;    /* Firefox, other Gecko */
    box-sizing: border-box;         /* Opera/IE 8+ */
}
.hyt_content input:focus{
	outline: none;
}
.hyt_content button{
	display: block;
	margin: 20px auto;
	display: inline-block;
	width: 120px;
	height: 50px;
	line-height: 50px;
	background: #ff5f5f;
	color: #fff;
	border: 0;
	border-radius: 8px;
	font-size: 18px;
	font-weight: 500;
	outline: none;
}
.hyt_content a.back_btn{
	display: block;
	margin: 20px auto;
	display: inline-block;
	width: 120px;
	height: 50px;
	line-height: 50px;
	background: #e0e0e0;
	color: #666666;
	border: 0;
	border-radius: 8px;
	font-size: 18px;
	font-weight: 500;
	text-decoration: none;
	margin-right: 10px;
}
.hyt_page .voucher_footer{
	position: fixed;
	bottom: 0;
	max-width: 375px;
}
.error_phone{
	text-align: left;
	padding:5px 0;
	color: #ff5f5f;
}
/* The Modal (background) */
.modal {
    display: none; /* Hidden by default */
    position: fixed; /* Stay in place */
    z-index: 999; /* Sit on top */
    left: 0;
    top: 0;
    width: 100%; /* Full width */
    height: 100%; /* Full height */
    overflow: auto; /* Enable scroll if needed */
    background-color: rgb(0,0,0); /* Fallback color */
    background-color: rgba(0,0,0,0.4); /* Black w/ opacity */
}
/*Modal header*/
.modal-header{
	position: relative;
	background: #fff;
	padding: 15px;
	border-bottom: 1px solid #e5e5e5; 
	border-radius: 6px 6px 0 0;
}
.modal-body{
	padding: 15px;
}
/* Modal Content/Box */
.modal-content {
    margin: 15% auto; /* 15% from the top and centered */
    border: 1px solid #888;
    width: 100%; /* Could be more or less, depending on screen size */
    max-width: 300px;
    background-color: #fff;
    -webkit-background-clip: padding-box;
    background-clip: padding-box;
    border: 1px solid #999;
    border: 1px solid rgba(0,0,0,.2);
    border-radius: 6px;
    outline: 0;
    -webkit-box-shadow: 0 3px 9px rgba(0,0,0,.5);
    box-shadow: 0 3px 9px rgba(0,0,0,.5);
}

/* The Close Button */
.modal-header .close {
    color: #aaa;
    float: right;
    font-size: 28px;
    font-weight: bold;
    position: absolute;
    right: 10px;
    top: 0;
}

.modal-header .close:hover,
.modal-header .close:focus {
    color: black;
    text-decoration: none;
    cursor: pointer;
}
</style>

	<script type="text/javascript">
		<?php
		$googleMaps = array();
		foreach ($stores as $store) {
		$googleMaps[] = array(
		'name'  => Translate::transObj($store, 'name'),
		'brand_name'  => Translate::transObj($store, 'brand_name'),
		'lat'   => $store->lat,
		'lng'   => $store->long,
		'address'   =>  Translate::transObj($store, 'address'),
		'phone' => $store->phone
		);
		}
		echo 'var mapStores = '.json_encode($googleMaps);
		?>

$(document).ready(function(){

	// Get the modal
	var modal = document.getElementById('noticeModal');
	// Get the <span> element that closes the modal
	var span = document.getElementsByClassName("close")[0];
	// When the user clicks on <span> (x), close the modal
	span.onclick = function() {
	    modal.style.display = "none";
	}

	// When the user clicks anywhere outside of the modal, close it
	window.onclick = function(event) {
	    if (event.target == modal) {
	        modal.style.display = "none";
	    }
	}

	$('.hyt img').click(function(){
		$(".map-wrap .map").css({"z-index":""});
		if($(window).width() > 414){
			var right = ($(window).width() - $("#voucher_wrapper").width())/2;
		}
		else{
			var right = 0;
		}
        $('.hyt_page').removeClass('active').animate({
            'right' : right
        },300);
        $('.hyt_page').css({'max-width':'375px'});
        $(".voucher_body").addClass('no-prevent');
        return false;

	});

	$('a.back_btn').click(function(){
		$('.hyt_page').addClass('active').animate({
		    'right' : '-100%'
		},300);
		$('.error_phone').html('');
		return false;
	});
	
	$('.submit_phone').click(function(e){
		var lang = $('html').attr('lang');
		var phone_val = $('.hyt_content input[name="phone"]').val();
		var err = '';
		if(phone_val.length == 0){
			if(lang == 'vi'){
				err += '<p>Vui lòng nhập số điện thoại</p>';
			}
			else{
				err += '<p>Please enter the phone number</p>';
			}
			
		}
		else if(phone_val.length > 11 || phone_val.length < 10){
			if(lang == 'vi'){
				err += '<p>Số điện thoại không được ít hơn 10 số hoặc nhiều hơn 11 số</p>';
			}
			else{
				err += '<p>Phone numbers must not be less than 10 digits or more than 11 digits</p>';
			}
			
		}
		else{
			var regExp = /^(09|08|01[2689])\d{8}$/; 
			var checkphone = phone_val.match(regExp);
			if (!checkphone) {
				if(lang == 'vi'){
					err += '<p>Số điện thoại không hợp lệ, vui lòng kiểm tra lại</p>';
				}
				else{
					err += '<p>Phone number is invalid, please check again</p>';
				}
			}
		}

		if(err != '' || err.length > 0){
			$('.error_phone').html('').append(err);
		}
		else{
			$('.error_phone').html('');
			var phone_number = $('.hyt_content input[name="phone"]').val();
			var voucher_code = $('.hyt_content input[name="voucher_code"]').val();
			var product_id = $('.hyt_content input[name="product_id"]').val();
			var price_id = $('.hyt_content input[name="price_id"]').val();
			var GotitRequest = {
	            Phone: phone_number ,
	            ProductID: product_id,
	            PriceID: price_id,
	            Code: voucher_code
	        };
			 var settings = {
	            async: true,
	            crossDomain: true,
	            url: "https://pso5iw0bpg.execute-api.ap-southeast-1.amazonaws.com/prod",
	            method: "POST",
	            contentType: "application/json; charset=utf-8",
	            dataType: "json",
	            headers: {
	                "Content-Type": "application/json"
	            },
	            data: JSON.stringify(GotitRequest)
	        }

	        
			$.ajax(settings).done(function (response) {
					if(response.MessageCode != 0){
						var new_param = $.extend(GotitRequest, response);
						//console.log(JSON.stringify(new_param));
						$.ajax({
			                type: 'POST',
			                url:'https://ys4w2vjpx1.execute-api.ap-southeast-1.amazonaws.com/prod/testing',
			                dataType: "json",
			                data: JSON.stringify(new_param),
			                success: function (data) {
			                	console.log('gotit');
			                	console.log(data);
			                	$('.hyt_content input[name="phone"]').val('')
								$('#noticeModal .modal-body p').text('<?php echo trans("content.hyt_success",["sdt" => "'+phone_number+'"])?>')
								modal.style.display = "block";
			                }
			            });
					}
					else{
						$('.hyt_content input[name="phone"]').val('')
						$('#noticeModal .modal-body p').text('<?php echo trans("content.hyt_success",["sdt" => "'+phone_number+'"])?>')
						modal.style.display = "block";
					}		
			});
		}
	});
	$('.hyt_form input').keyup(function() {
        var numbers = $(this).val();
        $(this).val(numbers.replace(/[^0-9.]/g, ''));
    });
});

function validate(evt) {
	  var theEvent = evt || window.event;
	  var key = theEvent.keyCode || theEvent.which;
	  key = String.fromCharCode( key );
	  var regex = /[0-9]|\./;
	  if( !regex.test(key) ) {
	    theEvent.returnValue = false;
	    if(theEvent.preventDefault) theEvent.preventDefault();
	  }
	}	
		
	</script>
	<script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyCjbeyHo86kWXc3nDZYK8q4AW5Joi0mvOY&v=3.exp&libraries=places"></script>

@endsection