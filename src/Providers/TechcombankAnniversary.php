<?php
namespace App\Issue\Techcombank\Providers;

class TechcombankAnniversary {

    public function __construct(){

    }

    /**
     * @author Ha Tran <manhhaniit@gmail.com>
     */
    public function view()
    {
        \App::register('App\Issue\Techcombank\TechcombankServiceProvider');
        return 'Techcombank::techcombank_anniversary';
    }

}