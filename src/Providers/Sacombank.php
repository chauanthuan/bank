<?php
// namespace App\Issue\Sacombank\Providers;
namespace Dayone\Issuer;
class Sacombank {

    public function __construct(){

    }

    /**
     * @author Ha Tran <manhhaniit@gmail.com>
     */
    public function view()
    {

        \App::register('Dayone\Issuer\SacombankServiceProvider');
        return 'Sacombank::index';
    }

}