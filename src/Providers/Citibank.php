<?php
namespace Dayone\Issuer;

class Citibank {

    public function __construct(){

    }

    /**
     * @author Ha Tran <manhhaniit@gmail.com>
     */
    public function view()
    {
        \App::register('Dayone\Issuer\CitibankServiceProvider');
        return 'Citibank::index';
    }

}