<?php
namespace Dayone\Issuer;

class Timo {

    public function __construct(){

    }

    /**
     * @author Ha Tran <manhhaniit@gmail.com>
     */
    public function view()
    {
        \App::register('Dayone\Issuer\TimoServiceProvider');
        return 'Timo::index';
    }

}