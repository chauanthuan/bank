<?php
namespace Dayone\Issuer;

class Eximbank {

    public function __construct(){

    }

    /**
     * @author Ha Tran <manhhaniit@gmail.com>
     */
    public function view()
    {
        \App::register('Dayone\Issuer\EximbankServiceProvider');
        return 'Eximbank::index';
    }

}