<?php
namespace Dayone\Issuer;

class JCB {

    public function __construct(){

    }

    /**
     * @author Ha Tran <manhhaniit@gmail.com>
     */
    public function view()
    {
        \App::register('Dayone\Issuer\JCBServiceProvider');
        return 'JCB::index';
    }

}